Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bully
Upstream-Contact: kimocoder <christian@aircrack-ng.org>
Source: https://github.com/kimocoder/bully

Files: *
License: GPL-3+
Copyright: 2012-2013, Brian Purcell <purcell.briand@gmail.com>
           2017 wiire <wi7ire@gmail.com>
           2020 kimocoder <christian@aircrack-ng.org>

Files: src/common/*
       src/crypto/*
       src/tls/*
       src/utils/*
       src/wps/*
License: GPL-2
Copyright: 2002-2009, Jouni Malinen <j@w1.fi>

Files: src/utils/radiotap.h
License: BSD-3-Clause
Copyright: 2003, 2004 David Young

Files: src/wps/wps_upnp.c
       src/wps/wps_upnp_event.c
       src/wps/wps_upnp_i.h
       src/wps/wps_upnp_ssdp.c
       src/wps/wps_upnp_web.c
License: BSD-3-Clause
Copyright: 2000-2003, Intel Corporation
           2006-2007 Sony Corporation
           2008-2009 Atheros Communications
           2009, Jouni Malinen <j@w1.fi>

Files: debian/*
License: GPL-2
Copyright: 2013 Mati Aharoni <muts@kali.org>
           2020 Sophie Brun <sophie@offensive-security.com>

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution.
 3. Neither the name of the copyright holder nor the names of its 
    contributors may be used to endorse or promote products derived from this 
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
